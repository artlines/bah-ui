import React, {Component} from 'react';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import App from "../App";

class Header extends Component{
    render(){
        return(
            <header className="MainHead">
                <Router>
                    <nav>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                        </ul>
                    </nav>
                    {/*<Route path="/" exact component={App} />*/}
                </Router>
            </header>
        )
    }
}

export default Header;
