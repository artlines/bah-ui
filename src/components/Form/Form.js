import React, {Component} from 'react';
import {withStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import './Form.css';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
});

class Form extends Component {

    handleForm = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);

        fetch('/api/add-agent', {
            method: 'POST',
            body: data,
        })
        .then((res) => res.json())
        .then((response) => console.log(response))
            .catch(error => console.warn(error))
    };

    render() {
        const { classes } = this.props;

        return (
            <Grid container spacing={20}>
                <Grid item xs={12}>
                    <Paper className="paper" elevation={1}>
                        <Typography variant="h5" component="h3">
                            Before Installing Make Sure
                        </Typography>
                        <Typography component="p">
                            <ul>
                                <li>You have java 8 or higher installed on your server</li>
                                <li>You specify a username which is an admin on your server</li>
                            </ul>
                        </Typography>
                    </Paper>

                    <form className="mainForm" onSubmit={this.handleForm}>
                        <TextField name="host" id="host" className={classes.textField} label="host" placeholder="mf-auto" m={20} />
                        <TextField name="username" id="username" className={classes.textField} label="username" placeholder="781assert" />
                        <TextField name="password" id="password" className={classes.textField} label="password" type="password" />
                        <Button className={classes.button} variant="contained" color="primary" type="submit">Install</Button>
                    </form>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(Form);
