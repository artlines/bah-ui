import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Form from "./components/Form/Form";
import Home from "./components/Home";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

class App extends Component {
  render() {
    return (
      <div className="bah-main">
        <header className="MainHead">
          <Router>
            <AppBar position="static" className="main-nav">
              <Toolbar>
                <Typography className="title" variant="h6" color="inherit">
                  Bamboo Agent Helper
                </Typography>
                <Button color="inherit"><Link to={'/'} className="nav-link">Home</Link></Button>
                <Button color="inherit"><Link to={'/form'} className="nav-link">Create New Agent</Link></Button>
              </Toolbar>
            </AppBar>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/form" component={Form} />
            </Switch>
          </Router>
        </header>
      </div>
    );
  }
}

export default App;
